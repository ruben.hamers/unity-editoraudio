# EditorAudioSource

Easy to use Editor audio source.

# Introduction
Unity3D does not support playing audio in the Editor (maybe for some of your custom plugins) by default.
So here is an AudioSource you can simply create through a constructor (so no monobehaviour) and play audio in your editor.
Note it is purposely set in an `Editor` directory to allow it only to be used in the editor because you should use the Unity3D [AudioSource](https://docs.unity3d.com/ScriptReference/AudioSource.html) at run-time!
Secondly, the class also contains the UnitTests so placing somewhere outside the Editor folder will break compilation. 
